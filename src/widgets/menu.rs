// SPDX-License-Identifier: GPL-3.0-or-later

use crate::store::{dispatch, store, State};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;

#[widget(@store extends gtk::Box)]
#[template(file = "menu.ui")]
pub struct MenuHeaderButton {
    #[template_child]
    pub back_button: TemplateChild<gtk::Button>,

    #[signal_handler(back_button clicked)]
    pub on_back: (),

    #[selector(state.history.queue)]
    pub select_history: (),
}

impl MenuHeaderButton {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create menu button")
    }
    pub fn constructed(&self) {
        //
    }

    fn on_back(&self, _: gtk::Button) {
        dispatch!(crate::store::BACK);
    }

    fn select_history(&self, state: &State) {
        self.back_button()
            .set_visible(state.history.queue.len() > 1)
    }
}

impl Default for MenuHeaderButton {
    fn default() -> Self {
        Self::new()
    }
}
