// SPDX-License-Identifier: GPL-3.0-or-later

use adw::subclass::prelude::{ActionRowImpl, PreferencesRowImpl};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::{prelude::*, subclass::list_box_row::ListBoxRowImpl};
use libadwaita as adw;
use std::cell::Cell;

#[widget(extends adw::ActionRow)]
#[template(file = "remove_row.ui")]
pub struct RemoveRow {
    #[template_child]
    pub label_widget: TemplateChild<gtk::Label>,
    #[template_child]
    pub remove_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub submit_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub cancel_button: TemplateChild<gtk::Button>,

    #[property_string]
    pub question: Cell<String>,
    #[property_string]
    pub label: Cell<String>,
    #[property_string]
    pub question_subtitle: Cell<String>,

    #[property_bool]
    pub needs_submit: Cell<bool>,

    /// Wait time for submitting in seconds.
    #[property_u64]
    pub wait_time: Cell<u64>,

    //
    #[signal]
    remove: (),
    #[signal]
    pre_remove: (),

    #[signal_handler(remove_button clicked)]
    on_remove: (),
    #[signal_handler(cancel_button clicked)]
    on_cancel: (),
    #[signal_handler(submit_button clicked)]
    on_submit: (),
}

pub trait RemoveRowImpl: PreferencesRowImpl {}

impl PreferencesRowImpl for imp::RemoveRow {}

impl ListBoxRowImpl for imp::RemoveRow {}

impl RemoveRow {
    pub fn new() -> Self {
        glib::Object::new(&[
            //
            ("needs_submit", &false),
            ("wait_time", &2u64),
            ("question", &""),
        ])
        .expect("Failed to create RemoveRow")
    }
    pub fn constructed(&self) {}

    pub fn needs_submit(&self) -> bool {
        self.property("needs_submit")
    }

    pub fn set_label(&self, label: &str) {
        self.set_property("label", label)
    }

    pub fn title(&self) -> String {
        self.property("title")
    }

    fn on_remove(&self, _: gtk::Widget) {
        self.emit_pre_remove();
        let title: String = self.property("title");
        let subtitle: String = self.property("subtitle");
        let question: String = self.property("question");
        let question_subtitle: String = self.property("question_subtitle");
        self.label_widget().set_visible(false);
        self.set_property("question", title);
        self.set_property("title", question);
        self.set_property("question_subtitle", subtitle);
        self.set_property("subtitle", question_subtitle);
        self.set_property("needs_submit", true);
        self.add_css_class("error");
    }
    fn on_cancel(&self, _: gtk::Button) {
        self.label_widget().set_visible(true);
        self.set_property("needs_submit", false);
        let label: String = self.property("label");
        self.label_widget().set_text(&label);
        let title: String = self.property("title");
        let subtitle: String = self.property("subtitle");
        let question: String = self.property("question");
        let question_subtitle: String = self.property("question_subtitle");
        self.set_property("question", title);
        self.set_property("title", question);
        self.set_property("question_subtitle", subtitle);
        self.set_property("subtitle", question_subtitle);
        self.remove_css_class("error");
    }
    fn on_submit(&self, b: gtk::Button) {
        self.on_cancel(b);
        self.emit_remove();
    }
}

impl Default for RemoveRow {
    fn default() -> Self {
        Self::new()
    }
}
