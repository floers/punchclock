// SPDX-License-Identifier: GPL-3.0-or-later

use crate::store::{dispatch, store, State};
use adw::traits::{ActionRowExt, PreferencesRowExt};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use glib::SignalHandlerId;
use gtk::{prelude::*, TemplateChild};
use libadwaita as adw;

#[widget(@store extends gtk::Box)]
#[template(file = "search.ui")]
pub struct Search {
    #[template_child]
    pub entry: TemplateChild<gtk::Entry>,
    #[template_child]
    pub entry_hidden: TemplateChild<gtk::Entry>,
    #[template_child]
    pub popover: TemplateChild<gtk::Popover>,
    #[template_child]
    pub task_list: TemplateChild<gtk::ListBox>,

    pub search_results_signal_handlers: std::cell::Cell<Vec<SignalHandlerId>>,

    #[selector(state.data.tasks)]
    select_tasks: (),
    #[selector(state.data.timings)]
    select_timings: (),

    pub set_text_internal: std::cell::Cell<bool>,
    #[signal_handler(entry changed)]
    pub on_change: (),

    #[signal_handler(entry_hidden activate)]
    pub on_hidden_activate: (),

    #[signal_handler(entry_hidden changed)]
    pub on_hidden_change: (),
}

impl Search {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create Search")
    }

    pub fn constructed(&self) {
        let key_controller = gtk::EventControllerKey::new();
        let task_list = self.task_list();

        key_controller.connect_key_pressed(
            glib::clone!(@weak task_list, => @default-return gtk::Inhibit(true), move |_, b, _, _| {
                if b == gdk4::Key::Down {
                    if let Some(child) = task_list.first_child() {
                        child.grab_focus();
                    }
                    gtk::Inhibit(false)
                } else {
                    gtk::Inhibit(true)
                }
            }),
        );
        self.entry_hidden().add_controller(&key_controller);
    }

    fn on_change(&self, entry: gtk::Entry) {
        let popover = self.popover();
        let entry_hidden = self.entry_hidden();
        let task_list = self.task_list();

        let internal = self.imp().set_text_internal.get();
        if internal {
            return;
        }

        if entry.text().is_empty() {
            let mut child = task_list.first_child();
            while let Some(c) = child {
                c.set_visible(true);
                child = c.next_sibling();
            }
            popover.set_visible(false);
            entry.grab_focus();
            return;
        }
        if !popover.is_visible() {
            popover.set_visible(true);
            entry_hidden.grab_focus();
            entry_hidden.select_region(
                entry_hidden.text_length() as i32,
                entry_hidden.text_length() as i32,
            );
        }
    }

    fn on_hidden_activate(&self, entry: gtk::Entry) {
        let text: String = entry.text().into();
        self.popover().set_visible(false);
        entry.grab_focus();
        dispatch!(crate::store::_ADD_TASK, text);
    }

    fn on_hidden_change(&self, entry: gtk::Entry) {
        let mut child = self.task_list().first_child();
        let mut at_least_one_visible = false;
        while let Some(c) = child {
            let r: adw::ActionRow = c.dynamic_cast().unwrap();
            let title_matches = r
                .title()
                .to_lowercase()
                .starts_with(&entry.text().to_lowercase());
            r.set_visible(title_matches);
            if title_matches {
                at_least_one_visible = true;
            }
            child = r.next_sibling();
        }
        self.task_list().set_visible(at_least_one_visible);
    }

    fn select_timings(&self, state: &crate::store::State) {
        let entry = self.entry();
        if let Some(timing) = state.data.timings.last() {
            if !entry.is_focus() && !self.popover().is_visible() {
                self.imp().set_text_internal.set(true);
                entry.set_text(timing.name());
                self.imp().set_text_internal.set(false);
            }
        }
    }

    fn select_tasks(&self, state: &crate::store::State) {
        let task_list = self.task_list();
        let tasks = &state.data.tasks;
        let entry = self.entry();
        let popover = self.popover();
        task_list.set_visible(!tasks.is_empty());

        let mut child = task_list.first_child();

        while let Some(c) = child {
            task_list.remove(&c);
            child = task_list.first_child();
        }
        for task in tasks {
            let row = adw::ActionRow::builder()
                .selectable(false)
                .activatable(true)
                .title(task)
                .build();
            let signal_handler_id =
                row.connect_activated(glib::clone!(@weak popover, @weak entry => move |row| {
                    entry.set_text(row.title().as_str());
                    dispatch!(crate::store::_SELECT_TASK, row.title().to_string());
                    popover.set_visible(false);
                    entry.grab_focus();
                }));
            let mut shs = self.imp().search_results_signal_handlers.take();
            shs.push(signal_handler_id);
            self.imp().search_results_signal_handlers.set(shs);
            task_list.append(&row);
        }
    }
}

impl Default for Search {
    fn default() -> Self {
        Self::new()
    }
}
