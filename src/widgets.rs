// SPDX-License-Identifier: GPL-3.0-or-later

use glib::StaticType;

mod pill;
pub use pill::*;
mod search;
pub use search::*;
mod clock;
pub use clock::*;
mod menu;
pub use menu::*;
mod task_row;
pub use task_row::*;
mod remove_row;
pub use remove_row::*;

// You need to call static_type() for all widgets you want to use in xml files. Otherwise GTK does not know about them.
pub fn init() {
    search::Search::static_type();
    clock::Clock::static_type();
    menu::MenuHeaderButton::static_type();
    remove_row::RemoveRow::static_type();
}
