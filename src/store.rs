// SPDX-License-Identifier: GPL-3.0-or-later

pub use gtk_rust_app::gstore;

pub use gstore::{dispatch, select, Action};

pub mod clock;
pub mod data;
pub mod history;

include!("../target/gra-gen/actions.rs");

// You can have 'private' actions (they will not be called from GTK elements directly).
// These action name should start with a dot.
pub const _CLEAR_NOTIFICATION: &str = ".clear-notification";
pub use clock::actions::*;
pub use data::actions::*;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct State {
    pub notification: Option<String>,
    pub data: data::Data,
    pub clock: clock::Clock,
    pub history: history::History,
}

gstore::store!(State);

pub fn reduce(action: &Action, state: &mut State) {
    clock::reduce(action, state);
    data::reduce(action, state);
    history::reduce(action, state);
}
