// SPDX-License-Identifier: GPL-3.0-or-later

use gtk::prelude::*;
use gtk_rust_app::widgets::Page;

pub const SETTINGS_PAGE_NAME: &str = "settings";

#[widget(extends gtk::Box)]
#[template(file = "settings.ui")]
pub struct Settings {}

impl Settings {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create settings page")
    }
    pub fn constructed(&self) {}
}

impl Page for Settings {
    fn name(&self) -> &'static str {
        SETTINGS_PAGE_NAME
    }

    fn title_and_icon(&self) -> Option<(String, String)> {
        None
    }
}

impl Default for Settings {
    fn default() -> Self {
        Self::new()
    }
}
