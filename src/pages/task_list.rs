// SPDX-License-Identifier: GPL-3.0-or-later

use crate::{
    store::{data::select_overall_time, dispatch, store, State},
    widgets::TaskRow,
};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::{prelude::*, TemplateChild};

pub const TASK_LIST_PAGE_NAME: &str = "task_list";

#[widget(@store extends gtk::Box)]
#[template(file = "task_list.ui")]
pub struct TaskList {
    #[template_child]
    pub task_list: TemplateChild<gtk::ListBox>,

    #[template_child]
    pub search: TemplateChild<gtk::Entry>,

    #[signal_handler(search changed)]
    on_search: (),
    #[signal_handler(search activate)]
    on_search_submit: (),

    #[selector(state.data)]
    select_tasks_and_timings: (),
}

impl gtk_rust_app::widgets::Page for TaskList {
    fn name(&self) -> &'static str {
        TASK_LIST_PAGE_NAME
    }

    fn title_and_icon(&self) -> Option<(String, String)> {
        None
    }
}

impl TaskList {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create Search")
    }

    pub fn constructed(&self) {}

    fn on_search_submit(&self, e: gtk::Entry) {
        let mut visible_task = None;
        let mut child = self.task_list().first_child();
        while let Some(c) = child {
            child = c.next_sibling();
            if let Ok(tr) = c.dynamic_cast::<TaskRow>() {
                if tr.is_visible() {
                    visible_task = Some(tr)
                }
            }
        }

        if let Some(task) = visible_task {
            dispatch!(crate::store::_SELECT_TASK, task.task());
        } else {
            dispatch!(crate::store::_ADD_TASK, e.text().to_string());
        }
        e.set_text("");
    }

    fn on_search(&self, e: gtk::Entry) {
        let mut child = self.task_list().first_child();
        while let Some(c) = child {
            child = c.next_sibling();
            if let Ok(tr) = c.dynamic_cast::<TaskRow>() {
                tr.set_visible(
                    tr.task()
                        .to_lowercase()
                        .starts_with(e.text().to_lowercase().as_str())
                        || e.text().is_empty(),
                );
            }
        }
    }

    fn select_tasks_and_timings(&self, state: &crate::store::State) {
        let task_list = self.task_list();
        let tasks = &state.data.tasks;

        for task in tasks {
            match get_task_row(task_list, task) {
                Some(row) => {
                    if row.needs_submit() {
                        continue;
                    }
                    if let Some(last) = state.data.timings.last() {
                        if row.task() == last.name() {
                            row.add_css_class("active_task");
                        } else {
                            row.remove_css_class("active_task");
                        }
                    }
                    let (h, m, s) = select_overall_time(state, task, false);
                    row.set_label(&format!("Today: {:02}:{:02}:{:02}", h, m, s));
                    let (h, m, s) = select_overall_time(state, task, true);
                    row.set_subtitle(&format!("Overall: {:02}:{:02}:{:02}", h, m, s));
                }
                None => {
                    let row = TaskRow::new(task);
                    let (h, m, s) = select_overall_time(state, task, false);
                    row.set_label(&format!("Today: {:02}:{:02}:{:02}", h, m, s));
                    let (h, m, s) = select_overall_time(state, task, true);
                    row.set_subtitle(&format!("Overall: {:02}:{:02}:{:02}", h, m, s));
                    task_list.append(&row);
                }
            }
        }

        // remove rows whose tasks do not exist anymore
        let mut child = self.task_list().first_child();
        while let Some(c) = child {
            child = c.next_sibling();
            if let Ok(tr) = c.dynamic_cast::<TaskRow>() {
                if !tasks.contains(&tr.task()) {
                    self.task_list().remove(&tr);
                }
            }
        }
    }
}

fn get_task_row(task_list: &gtk::ListBox, task: &String) -> Option<TaskRow> {
    let mut task_row = None;
    let mut child = task_list.first_child();
    while let Some(c) = child {
        child = c.next_sibling();
        if let Ok(tr) = c.dynamic_cast::<TaskRow>() {
            if &tr.task() == task {
                task_row = Some(tr);
                break;
            }
        }
    }
    task_row
}

impl Default for TaskList {
    fn default() -> Self {
        Self::new()
    }
}
