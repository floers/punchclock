// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::HashSet,
    fs::{create_dir_all, File},
    io::{BufRead, BufReader, Write},
};

use crate::domain::Timing;

use super::{dispatch, store, Action, State};

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Data {
    pub tasks: HashSet<String>,
    pub timings: Vec<Timing>,
}

pub mod actions {
    pub const _ADD_TASK: &str = ".add-task";
    pub const _SAVE: &str = ".save";
    pub const _SELECT_TASK: &str = ".select-task";
    pub const _DELETE: &str = ".delete";

    pub const _INIT: &str = ".init";
}

pub fn reduce(action: &Action, state: &mut State) {
    match action.name() {
        actions::_ADD_TASK => {
            let task = action.arg().unwrap();
            state.data.tasks.insert(task);
        }
        actions::_SELECT_TASK => {
            let name = action.arg().unwrap();
            state.data.timings.push(Timing::new(name, 0));
        }
        actions::_INIT => unsafe {
            match DATA.take() {
                Some(data) => state.data = data,
                None => eprintln!("Failed to initialize state."),
            }
        },
        actions::_DELETE => {
            let task: String = action.arg().unwrap();
            state.data.tasks = state
                .data
                .tasks
                .iter()
                .cloned()
                .filter(|t| t != &task)
                .collect();
            state.data.timings = state
                .data
                .timings
                .iter()
                .cloned()
                .filter(|t| t.name() != task)
                .collect();
        }
        crate::store::QUIT => {
            save_data(&state.data);
            backup_data();
        }
        _ => {}
    }
}

#[derive(Debug, Default)]
pub(crate) struct SelectionMiddleware;
impl SelectionMiddleware {
    pub(crate) fn new() -> Box<Self> {
        Default::default()
    }
}

static mut DATA: Option<Data> = None;

impl super::gstore::Middleware<crate::store::State> for SelectionMiddleware {
    fn pre_reduce(&self, action: &Action, state: &crate::store::State) {
        if action.name() == crate::store::_DELETE {
            let task: String = action.arg().unwrap();
            if let Some(current_timing) = state.data.timings.last() {
                if current_timing.name() == task {
                    dispatch!(crate::store::_STOP)
                }
            }
            let task: String = action.arg().unwrap();
            dispatch!(crate::store::_SELECT_TASK, task);
        }
    }
    fn post_reduce(&self, action: &super::gstore::Action, _: &crate::store::State) {
        if action.name() == crate::store::_ADD_TASK {
            let task: String = action.arg().unwrap();
            dispatch!(crate::store::_SELECT_TASK, task);
            dispatch!(crate::store::_START);
        }
    }
}

const APP_DIR: &str = "punchclock";
pub const TIMINGS_FILE_NAME: &str = "timings.csv";
pub const TIMINGS_BACKUP_FILE_NAME: &str = "timings.backup.csv";
pub const TASKS_FILE_NAME: &str = "tasks";
pub const TASKS_BACKUP_FILE_NAME: &str = "tasks.backup";

#[derive(Debug, Default)]
pub(crate) struct PersistenceMiddleware;
impl PersistenceMiddleware {
    pub(crate) fn new() -> Box<Self> {
        Default::default()
    }
}

impl super::gstore::Middleware<crate::store::State> for PersistenceMiddleware {
    fn pre_reduce(&self, action: &Action, _s: &crate::store::State) {
        if action.name() == actions::_INIT {
            unsafe { DATA = load_data() }
        }
    }

    fn post_reduce(&self, action: &super::gstore::Action, state: &crate::store::State) {
        if action.name() == crate::store::_ADD_TASK {
            save_data(&state.data)
        }
        if action.name() == crate::store::_SAVE {
            save_data(&state.data)
        }
        if action.name() == crate::store::OPEN_APP_DIR {
            if let Err(e) = std::process::Command::new("xdg-open")
                .arg(app_dir())
                .output()
            {
                error!("Error: {}", e)
            }
        }
    }
}

pub fn select_overall_time(state: &State, name: &str, overall: bool) -> (u64, u64, u64) {
    let today = time::OffsetDateTime::now_utc().date();
    let overall_time = state
        .data
        .timings
        .iter()
        .filter(|t| t.name() == name)
        .filter(|t| overall || t.start().date() == today)
        .fold(0, |acc, x| acc + x.time());
    let h = overall_time / 60 / 60;
    let m = (overall_time / 60).rem_euclid(60);
    let s = overall_time.rem_euclid(60);
    (h, m, s)
}

fn app_dir() -> std::path::PathBuf {
    let mut app_dir = glib::user_data_dir();
    app_dir.push(APP_DIR);
    app_dir
}

fn backup_data() {
    let app_dir = app_dir();

    if !app_dir.exists() {
        if let Err(e) = create_dir_all(&app_dir) {
            error!("Could not create app dir: {}", e);
            return;
        }
    }

    let tasks_file_path = app_dir.join(TASKS_FILE_NAME);
    let timings_file_path = app_dir.join(TIMINGS_FILE_NAME);

    let tasks_backup_file_path = app_dir.join(TASKS_BACKUP_FILE_NAME);
    let timings_backup_file_path = app_dir.join(TIMINGS_BACKUP_FILE_NAME);

    // no need to backup
    if !tasks_file_path.exists() || !timings_file_path.exists() {
        return;
    }

    std::fs::copy(timings_file_path, timings_backup_file_path)
        .expect("Failed to write backup files.");
    std::fs::copy(tasks_file_path, tasks_backup_file_path).expect("Failed to write backup files.");
}

fn save_data(data: &Data) {
    let mut app_dir = glib::user_data_dir();
    app_dir.push(APP_DIR);

    if !app_dir.exists() {
        if let Err(e) = create_dir_all(&app_dir) {
            error!("Could not create app dir: {}", e);
            return;
        }
    }

    let mut tasks_file =
        File::create(app_dir.join(TASKS_FILE_NAME)).expect("Could not create app state file.");
    let mut timings_file =
        File::create(app_dir.join(TIMINGS_FILE_NAME)).expect("Could not create app state file.");

    for task in &data.tasks {
        writeln!(tasks_file, "{}", task).unwrap();
    }

    let mut writer = csv::Writer::from_writer(&mut timings_file);
    for timing in &data.timings {
        writer.serialize(timing).unwrap();
    }
}

fn load_data() -> Option<Data> {
    let mut app_dir = glib::user_data_dir();
    app_dir.push(APP_DIR);

    if !app_dir.exists() {
        return None;
    }

    let mut data: Data = Default::default();

    let tasks_file = File::open(app_dir.join(TASKS_FILE_NAME));
    if tasks_file.is_err() {
        return None;
    }
    let tasks_file = tasks_file.unwrap();

    let tasks_reader = BufReader::new(tasks_file);
    for task in tasks_reader.lines().flatten() {
        data.tasks.insert(task);
    }

    let timings_file = File::open(app_dir.join(TIMINGS_FILE_NAME));
    if timings_file.is_err() {
        return None;
    }
    let timings_file = timings_file.unwrap();

    let mut timings_reader = csv::Reader::from_reader(timings_file);
    for timing in timings_reader.deserialize::<Timing>().flatten() {
        data.timings.push(timing);
    }

    Some(data)
}
