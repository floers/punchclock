#!/bin/bash
if [ -z "$1" ]; then
    echo "Please provide tag to create the changelog for as argument."
    exit 1
fi

cog changelog -a $1 --template ./ci/flatpak-changelog.tera
