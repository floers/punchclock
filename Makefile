install-gsettings:
	install -D target/gra-gen/codes.loers.Punchclock.gschema.xml /usr/share/glib-2.0/schemas/codes.loers.Punchclock.gschema.xml
	glib-compile-schemas /usr/share/glib-2.0/schemas

uninstall-gsettings:
	rm /usr/share/glib-2.0/schemas/codes.loers.Punchclock.gschema.xml
	glib-compile-schemas /usr/share/glib-2.0/schemas
