[app]
flatpak-runtime-version = "43"
id = "codes.loers.Punchclock"

name = "Punchclock"
# A generic name. E.g. if your calculator project name is
# foo-calc you still want your distro to show the app as "Calculator".
generic-name = "Punchclock"

# License for metadata.
metadata-license = "CC0-1.0"

# Short summary of what your app does. 
# Will be shown from distros for the app as well. 
# Is also included in the store page.
summary = "Track time for your tasks."

# Description text for your app.
# You can make use **some** HTML tags
# See here: 
# - https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-description
# - https://www.freedesktop.org/software/appstream/docs/chap-CollectionData.html#tag-ct-description
description = """
<p>Track time for your tasks.</p>
"""

# Desktop file categories. 
# See https://specifications.freedesktop.org/menu-spec/menu-spec-1.0.html#category-registry
categories = ["GTK", "Office", "Utility"]

# Possible display sizes and how your app can be interacted with
# See https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-relations
requires = [{ display = ">360" }]
recommends = [
  { control = "pointer" },
  { control = "keyboard" },
  { control = "touch" },
]

# Screenshots that will be shown in the store page. The default screenshot will be the first screen to show in the store.
# See https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-screenshots
screenshots = [
  { type = "default", url = "https://gitlab.com/floers/punchclock/-/raw/main/screenshots/small.png" },
  { url = "https://gitlab.com/floers/punchclock/-/raw/main/screenshots/large.png" },
]

# The release history
releases = [
  # this_comment_is_required_for_automated_release
 { version = "0.0.6", date = "2023-02-09", description = """
<p>Bug Fixes</p><ul><li>Increase minimal clock width</li></ul><p>Documentation</p><ul><li>Remove liberapay note</li></ul> """ },
 { version = "0.0.5", date = "2022-12-29", description = """
<p>Bug Fixes</p><ul><li>prevent about screen from exceeding small screen width</li></ul><p>Miscellaneous Chores</p><ul><li>Update app icon</li><li>Add app license and maintainer info and required display size</li></ul> """ },
 { version = "0.0.4", date = "2022-12-27", description = """
<p>Bug Fixes</p><ul><li>Remove unimplemented settings menu option</li></ul><p>Documentation</p><ul><li>Add homepage and repo urls to manifests</li></ul><p>Miscellaneous Chores</p><ul><li>Update to gnome 43 runtime</li></ul> """ },
 { version = "0.0.3", date = "2022-12-25", description = """
<p>Miscellaneous Chores</p><ul><li>Upload flatpak artifacts during release</li></ul> """ },
  { version = "0.0.2", date = "2022-12-24", description = """
<p>Bug Fixes</p><ul><li>Stop timer when removing the current task</li></ul> """ },
  { version = "0.0.1", date = "2022-12-23", description = """
    Initial version
  """ },
]

# Flatpak permissions of your app
# See https://docs.flatpak.org/en/latest/sandbox-permissions.html
permissions = [
  "share=network",
  "socket=fallback-x11",
  "share=ipc",
  "socket=wayland",
  "device=dri",
  "socket=pulseaudio",
]

# Define your apps global settings
[settings]
window-width = 400
window-height = 400

# Define gtk actions. See the [GTK Rust book](https://gtk-rs.org/gtk4-rs/stable/latest/book/actions.html) for further information.
[actions]
quit = { accelerators = ["<primary>W"] }
navigate = { type = "s" }
back = { accelerators = ["<alt>Left"] }
open-app-dir = { accelerators = ["<alt>O"] }
